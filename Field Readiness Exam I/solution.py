#
#Task 1
#Open the exam.txt file with Python and with only read permission and store the contents in a list called exam_lines

myfile = open('exam.txt','r')
lines = myfile.read()
print (lines)
myfile.close()

# Your output should look something like this:
#['Welcome to your First Exam Recruit.\n', 'Only the best recruits can become agents.\n', 'Do you have what it takes?\n', 'We will test your knowledge with this field readiness exam.\n', 'It should be pretty simple, since you only know the basics so far.\n', "Let's get started.\n", 'Best of luck recruit.']


#Task 2
#How many lines does this file have?

7

# Your output should look something like this:
#7


#Task 3
#Print out the 5th line of the text file.

myfile = open('exam.txt','r')
lines = myfile.readlines()
print (lines[4])
myfile.close()

# Your output should look something like this:
#It should be pretty simple, since you only know the basics so far.


#Task 4
#Grab the last line of the text file and save it to a variable called last.

# Your output should look something like this:
#'Best of luck recruit.'


#Task 5
#Use indexing to grab the letter 'o' from the last line of the file.

# Your output should look something like this:
#'o'


#Task 6
#How could you use Python to count how many words there are in the last line?

# Your output should look something like this:
#4


#Task 7
#What data types are returned by the following lines of code:
#
#1.)  2/3
#2.)  2 + 2.0
#3.)  1 + 1
#4.)  "2" + "2"
#5.)  1 > 2
#Try to do these in your head before checking by running the code in python.


# No need to write down your answers, just use this cell to self check is necessary.


#Task 8
#Let's check how well you understand indexing and key calls. Here we present to you a set of dictionaries and lists that are nested inside a single dictionary d. While this is an unrealistic representation of how you would use these data types in the field, this is just for practice:

#d = {"levelone":[1,2,{'leveltwo':[5,6,[1,['get me please']]]}]}
#Your task is to retrieve the string "get me please" from the dictionary with stacked index and key calls.

#Hint: Approach this step by step, slowly adding on more and more index calls. Recall that you can use .keys() to figure out what keys are in a dictionary.

# Your output should look something like this:
#'get me please'


#Bonus Task
#How many unique integers are in this list? (You will need to use a casting method we haven't shown you yet)

#mylist = [1,2,3,4,5,6,4,3,2,1,2,3,4,5,6,6,7,8,5,6,7,8,9,8,9,8,9,7,10,123,1,2,2,3,1,3,2,4,1,4,4,1,2,2,22,3,4,1,4,1]

# Your output should look something like this:
#12
#Excellent work recruit, now that you are done with the basics of data types and storage with Python, let's move on to learning about control flow with Python.
